from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), index=True, unique=False)
    email = db.Column(db.String(120), index=True, unique=True)
    blessed = db.Column(db.String(64), index=True, unique=False)
    admin = db.Column(db.String(64), index=True, unique=False)
    idtoken = db.Column(db.String(2000), index=True, unique=True)

    def __repr__(self):
        return '<User %r>' % (self.name)
