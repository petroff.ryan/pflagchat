###########################################################
# Code below written after Architecture Design's patterns #
# This code will be happening in response to socket events, and will live in events.py #
###########################################################

# Code that still needs writing:
#     Error handling (for unexpected/unreported disconnections)
#     Events should include confirmations so missed messages can be repeated and not lost

from flask import request, session
from flask_socketio import emit, join_room, leave_room, rooms
from .. import socketio

CALLERS_STACK = []
VOLUNTEERS_STACK = []
ACTIVE_CHATS = []

def attempt_chat_pairing():
    '''Pair up a Caller and an Volunteer in a room to chat'''
    if CALLERS_STACK and VOLUNTEERS_STACK:
        callerid = CALLERS_STACK.pop(0)
        volunteerid = VOLUNTEERS_STACK.pop(0)
        room = callerid + '#' + volunteerid
        ACTIVE_CHATS.append(room)
        socketio.server.enter_room(callerid, room)
        socketio.server.enter_room(volunteerid, room)
        emit('enter_chat', {'room':room}, room=room)

def destroy_chat_room(room):
    '''Destroy rooms when their time has come'''
    emit('destroy_room', {'room':room}, room=room)
    callerid = room.split('#')[0]
    volunteerid = room.split('#')[1]
    socketio.server.leave_room(callerid, room)
    socketio.server.leave_room(volunteerid, room)
    ACTIVE_CHATS.remove(room)
    VOLUNTEERS_STACK.append(volunteerid)
    attempt_chat_pairing()

@socketio.on('caller_connects')
def on_caller_connects():
    session['callerid'] = request.sid
    session['role'] = 'caller'
    CALLERS_STACK.append(session['callerid'])
    attempt_chat_pairing()

@socketio.on('caller_disconnects')
def on_caller_disconnects(room):
    destroy_chat_room(room)

@socketio.on('caller_speaks')
def on_caller_speaks(message, room):
    emit('message', {'msg':  message['msg'], 'room': room['room'], 'name': session['callerid'], 'role': session['role']}, room=room['room'])

@socketio.on('volunteer_connects')
def on_volunteer_connects():
    session['volunteerid'] = request.sid  # or fetch the volunteer id from the database if this is not good enough
    session['role'] = 'volunteer'
    VOLUNTEERS_STACK.append(session['volunteerid'])
    attempt_chat_pairing()

@socketio.on('volunteer_disconnects')
def on_volunteer_disconnects(room):
    destroy_chat_room(room)
    # check if volunteer closed a single room, or died, closing all

@socketio.on('volunteer_speaks')
def on_volunteer_speaks(message, room):
    emit('message', {'msg':  message['msg'], 'room': room['room'], 'name': session['volunteerid'], 'role': session['role']}, room=room['room'])

@socketio.on('volunteer_requests_new_caller')
def on_volunteer_requests_new_caller():
    VOLUNTEERS_STACK.append(session['volunteerid'])
    attempt_chat_pairing()


# Notes
# All clients are assigned a room when they connect, named with the session ID of the connection, which can be obtained from request.sid
# Since all clients are assigned a personal room, to address a message to a single client, the session ID of the client can be used as the room argument.
# from https://flask-socketio.readthedocs.io/en/latest/
