import json
import logging
from oauth2client import client, crypt
from sqlalchemy.orm.exc import NoResultFound

from flask import session, request, render_template, redirect, url_for

from . import main
from app import db, models


@main.route('/', methods=['GET', 'POST'])
def index():
    """"Visitors first page"""
    if request.method == 'GET':
        return render_template('caller-greet.html')
    if request.method == 'POST':
        return render_template('caller-chat.html')


@main.route('/volunteer', methods=['GET', 'POST'])
def volunteer_welcome():
    if request.method == 'GET':
        return render_template('volunteer-greet.html')
    if request.method == 'POST':
        return render_template('volunteer-chat.html')


@main.route('/login', methods=['GET', 'POST'])
def login_volunteer():
    if request.method == 'GET':
        return render_template('login.html')

    if request.method == 'POST':
        logging.warning(request.form.get('name'))
        email = request.form.get('email')
        u = models.User(name=request.form.get('name'),
                        email=email,
                        blessed='notyet',
                        admin='notyet',
                        idtoken=request.form.get('idtoken'))
        user_exists = db.session.query(models.User).filter_by(email=email).one_or_none()
        print 'user_exists: ', user_exists
        # TODO: Handle case where the following line fails. info here: https://developers.google.com/identity/sign-in/web/backend-auth
        # TODO: Also use the info sent back to server (name, email) from google,
        # instead of client's computer sending it back to us.
        idinfo = client.verify_id_token(request.form.get('idtoken'), None)
        if idinfo['iss'] not in ['accounts.google.com',
                                 'https://accounts.google.com']:
            raise crypt.AppIdentityError("Wrong issuer.")
        if user_exists:
        #this should really be checking the idtoken!
            logging.warning('user already in DB!')
            if (user_exists.blessed == 'notyet'):
                logging.warning('This initiate remains unblessed')
                return render_template('volunteer-awaitblessing.html')
            else:
                logging.warning('This blessed initiate is ready to serve the cause')
                session['name'] = 'Volunteer'
                return redirect(url_for('main.volunteer_welcome'))
        else:
            db.session.add(u)
            db.session.commit()
            logging.warning('This initiate awaits a blessing')
            return render_template('volunteer-awaitblessing.html')


@main.route('/admin', methods=['GET', 'POST'])
def login_admin():
    if request.method == 'GET':
        return render_template('admin-login.html')

    if request.method == 'POST':
    #    logging.warning('Login page debug is live')
        logging.warning(request.form.get('name'))
    #    logging.warning(request.form.get('email'))
    #    logging.warning(request.form.get('idtoken'))
        email = request.form.get('email')
        user_exists = db.session.query(models.User).filter_by(email=email, admin='fulladmin').one_or_none()
        if user_exists:

        #this should really be checking the idtoken!
            users_list = db.session.query(models.User).all()
            users_list_organized = [
                {
                    'name': user.name,
                    'email': user.email,
                    'blessed': user.blessed,
                    'admin': user.admin
                } for user in users_list]

            return render_template('admin-greet.html', users=users_list_organized)
        else:
            #log a warning that there was a login attempt
            return render_template('something-went-awry.html')


@main.route('/admin_action', methods=['POST'])
def admin_action():

    action_type = request.form.get('action_type')
    targetted_user = request.form.get('targetted_user')
    grant = json.loads(request.form.get('grant'))

    try:
        user = db.session.query(models.User).filter_by(email=targetted_user).one()
    except NoResultFound:
        logging.warning('no user found with email {0}!'.format(targetted_user))
        redirect(url_for('main.login_admin'))

    # Put all logging as warning because info doens't show up in console
    if action_type == 'blessing' and grant and user.blessed == 'notyet':
        user.blessed = 'blessed'
        logging.warning('blessed user {0}!'.format(targetted_user))
    elif action_type == 'blessing' and not grant and user.blessed == 'blessed':
        user.blessed = 'notyet'
        logging.warning('revoked access to user {0}!'.format(targetted_user))

    db.session.commit()

    # TODO: Separate login_admin from admin_greet
    return redirect(url_for('main.login_admin'))
